USE [master]
GO
/****** Object:  Database [RHIT_Club_App]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE DATABASE [RHIT_Club_App]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RHIT_Club_App', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.TITAN\MSSQL\DATA\RHIT_Club_App.mdf' , SIZE = 20480KB , MAXSIZE = 51200KB , FILEGROWTH = 10%)
 LOG ON 
( NAME = N'RHIT_Club_App_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.TITAN\MSSQL\DATA\RHIT_Club_App_log.ldf' , SIZE = 5120KB , MAXSIZE = 20480KB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RHIT_Club_App] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RHIT_Club_App].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RHIT_Club_App] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET ARITHABORT OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RHIT_Club_App] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RHIT_Club_App] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RHIT_Club_App] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RHIT_Club_App] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET RECOVERY FULL 
GO
ALTER DATABASE [RHIT_Club_App] SET  MULTI_USER 
GO
ALTER DATABASE [RHIT_Club_App] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RHIT_Club_App] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RHIT_Club_App] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RHIT_Club_App] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [RHIT_Club_App]
GO
/****** Object:  User [yangz1]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE USER [yangz1] FOR LOGIN [yangz1] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [morschm]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE USER [morschm] FOR LOGIN [morschm] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [clubUser]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE USER [clubUser] FOR LOGIN [clubUser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [yangz1]
GO
ALTER ROLE [db_owner] ADD MEMBER [morschm]
GO
ALTER ROLE [db_datareader] ADD MEMBER [morschm]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [morschm]
GO
ALTER ROLE [db_owner] ADD MEMBER [clubUser]
GO
/****** Object:  Table [dbo].[Club]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Club](
	[club_name] [varchar](50) NOT NULL,
	[club_type] [varchar](16) NOT NULL,
	[description] [varchar](200) NULL,
 CONSTRAINT [PK_Club] PRIMARY KEY CLUSTERED 
(
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Event]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Event](
	[event_id] [int] NOT NULL,
	[title] [varchar](100) NOT NULL,
	[description] [varchar](200) NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Hold]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hold](
	[club_name] [varchar](50) NOT NULL,
	[event_id] [int] NOT NULL,
 CONSTRAINT [PK_Hold] PRIMARY KEY NONCLUSTERED 
(
	[club_name] ASC,
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Hold_Event]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE CLUSTERED INDEX [IX_Hold_Event] ON [dbo].[Hold]
(
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manage]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Manage](
	[rose_username] [varchar](10) NOT NULL,
	[club_name] [varchar](50) NOT NULL,
	[title] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Manage] PRIMARY KEY NONCLUSTERED 
(
	[rose_username] ASC,
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CIX_Manage]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE CLUSTERED INDEX [CIX_Manage] ON [dbo].[Manage]
(
	[rose_username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Media File]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Media File](
	[file_id] [int] NOT NULL,
	[file_path] [varchar](256) NULL,
	[file_type] [varchar](10) NULL,
	[club_name] [varchar](50) NULL,
 CONSTRAINT [PK_Media File] PRIMARY KEY CLUSTERED 
(
	[file_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_Media_File_FIle_Path] UNIQUE NONCLUSTERED 
(
	[file_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Member_Of]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member_Of](
	[rose_username] [varchar](10) NOT NULL,
	[club_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Member_Of] PRIMARY KEY NONCLUSTERED 
(
	[rose_username] ASC,
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CIX_Member_Of]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE CLUSTERED INDEX [CIX_Member_Of] ON [dbo].[Member_Of]
(
	[rose_username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reserve]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reserve](
	[time_start] [smalldatetime] NOT NULL,
	[time_end] [smalldatetime] NOT NULL,
	[room_number] [varchar](20) NOT NULL,
	[event_id] [int] NOT NULL,
 CONSTRAINT [PK_Reserve] PRIMARY KEY NONCLUSTERED 
(
	[event_id] ASC,
	[time_start] ASC,
	[time_end] ASC,
	[room_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_Reserve_Event_Id]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE CLUSTERED INDEX [IX_Reserve_Event_Id] ON [dbo].[Reserve]
(
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Room](
	[room_number] [varchar](20) NOT NULL,
	[building] [varchar](20) NOT NULL,
	[event_id] [int] NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[room_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sign_Up]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sign_Up](
	[rose_username] [varchar](10) NOT NULL,
	[event_id] [int] NOT NULL,
 CONSTRAINT [PK_Sign_Up] PRIMARY KEY NONCLUSTERED 
(
	[rose_username] ASC,
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Sign_Up_Username]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE CLUSTERED INDEX [IX_Sign_Up_Username] ON [dbo].[Sign_Up]
(
	[rose_username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subscribe]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe](
	[rose_username] [varchar](10) NOT NULL,
	[club_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Subscribe] PRIMARY KEY NONCLUSTERED 
(
	[rose_username] ASC,
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[rose_username] [varchar](10) NOT NULL,
	[email] [varchar](30) NOT NULL,
	[name] [varchar](70) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[rose_username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_User] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Sign_Up_Events_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Sign_Up_Events_View]
AS
SELECT dbo.[User].rose_username, dbo.[User].email, dbo.[User].name, dbo.Event.event_id
FROM  dbo.[User] CROSS JOIN
         dbo.Event

GO
/****** Object:  View [dbo].[User_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[User_View]
AS
SELECT dbo.[User].*
FROM  dbo.[User]

GO
/****** Object:  View [dbo].[Event_List_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Event_List_View]
AS
SELECT dbo.Event.title, dbo.Event.description, dbo.Reserve.time_start AS Start_Time, dbo.Reserve.time_end AS End_Time, dbo.Room.room_number, dbo.Room.building, 
         CASE WHEN dbo.Sign_Up_Events_View.rose_username = dbo.User_View.rose_username THEN '1' WHEN dbo.Sign_Up_Events_View.rose_username != dbo.User_View.rose_username THEN '0' END AS SignedUp
FROM  dbo.Event INNER JOIN
         dbo.Reserve ON dbo.Event.event_id = dbo.Reserve.event_id INNER JOIN
         dbo.Room ON dbo.Reserve.room_number = dbo.Room.room_number CROSS JOIN
         dbo.Sign_Up_Events_View CROSS JOIN
         dbo.User_View

GO
/****** Object:  View [dbo].[Club_in_Charge_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Club_in_Charge_View]
AS
SELECT club_name, title, rose_username
FROM  dbo.Manage

GO
/****** Object:  View [dbo].[Club_List_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Club_List_View]
AS
SELECT dbo.Club.club_name, dbo.Club.club_type, dbo.Club.description, dbo.Subscribe.rose_username AS SubscribeUser, dbo.Member_Of.rose_username AS MemberUser, CASE WHEN dbo.Member_Of.rose_username = dbo.[Manage].rose_username THEN 1 ELSE 0 END AS Officer
FROM  dbo.Club INNER JOIN
         dbo.Subscribe ON dbo.Club.club_name = dbo.Subscribe.club_name INNER JOIN
         dbo.Member_Of ON dbo.Club.club_name = dbo.Member_Of.club_name AND dbo.Subscribe.rose_username = dbo.Member_Of.rose_username CROSS JOIN
         dbo.Manage

GO
/****** Object:  View [dbo].[Member_Of_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Member_Of_View]
AS
SELECT rose_username, email, name
FROM  dbo.[User]

GO
/****** Object:  View [dbo].[Member_Or_Subscribe_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Member_Or_Subscribe_View]
AS
SELECT dbo.[User].email, dbo.[User].rose_username, dbo.[User].name, CASE WHEN dbo.[User].rose_username = dbo.Member_Of.rose_username THEN '1' WHEN dbo.[User].rose_username != dbo.Member_Of.rose_username THEN '0' END AS isMember
FROM  dbo.[User] INNER JOIN
         dbo.Member_Of ON dbo.[User].rose_username = dbo.Member_Of.rose_username
GROUP BY dbo.[User].email, dbo.[User].rose_username, dbo.[User].name, dbo.Member_Of.rose_username

GO
/****** Object:  View [dbo].[Sign_Up_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Sign_Up_View]
AS
SELECT club_name
FROM  dbo.Sign_Up

GO
/****** Object:  View [dbo].[Subscribe_To_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Subscribe_To_View]
AS
SELECT rose_username, email, name
FROM  dbo.[User]

GO
/****** Object:  View [dbo].[Subscribe_View]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Subscribe_View]
AS
SELECT club_name
FROM  dbo.Sign_Up

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Club]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Club] ON [dbo].[Club]
(
	[club_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Hold_Club_Name]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Hold_Club_Name] ON [dbo].[Hold]
(
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Manage]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Manage] ON [dbo].[Manage]
(
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Media File_Club_Name]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Media File_Club_Name] ON [dbo].[Media File]
(
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Media File_Club_Name_File_Type]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Media File_Club_Name_File_Type] ON [dbo].[Media File]
(
	[club_name] ASC,
	[file_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Member_Of]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Member_Of] ON [dbo].[Member_Of]
(
	[club_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Reserve_Room_Number]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Reserve_Room_Number] ON [dbo].[Reserve]
(
	[room_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Reserve_Time_End]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Reserve_Time_End] ON [dbo].[Reserve]
(
	[time_end] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Reserve_Time_Start]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Reserve_Time_Start] ON [dbo].[Reserve]
(
	[time_start] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Sign_Up_Event_Id]    Script Date: 2/17/2017 5:06:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_Sign_Up_Event_Id] ON [dbo].[Sign_Up]
(
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Hold]  WITH CHECK ADD  CONSTRAINT [FK_Hold_Club_Name] FOREIGN KEY([club_name])
REFERENCES [dbo].[Club] ([club_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Hold] CHECK CONSTRAINT [FK_Hold_Club_Name]
GO
ALTER TABLE [dbo].[Hold]  WITH CHECK ADD  CONSTRAINT [FK_Hold_Event_Id] FOREIGN KEY([event_id])
REFERENCES [dbo].[Event] ([event_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Hold] CHECK CONSTRAINT [FK_Hold_Event_Id]
GO
ALTER TABLE [dbo].[Manage]  WITH CHECK ADD  CONSTRAINT [FK_Manage_Club] FOREIGN KEY([club_name])
REFERENCES [dbo].[Club] ([club_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Manage] CHECK CONSTRAINT [FK_Manage_Club]
GO
ALTER TABLE [dbo].[Manage]  WITH CHECK ADD  CONSTRAINT [FK_Manage_User] FOREIGN KEY([rose_username])
REFERENCES [dbo].[User] ([rose_username])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Manage] CHECK CONSTRAINT [FK_Manage_User]
GO
ALTER TABLE [dbo].[Media File]  WITH CHECK ADD  CONSTRAINT [FK_Media File_Club_Name] FOREIGN KEY([club_name])
REFERENCES [dbo].[Club] ([club_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Media File] CHECK CONSTRAINT [FK_Media File_Club_Name]
GO
ALTER TABLE [dbo].[Member_Of]  WITH CHECK ADD  CONSTRAINT [FK_Member_Of_User] FOREIGN KEY([rose_username])
REFERENCES [dbo].[User] ([rose_username])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Member_Of] CHECK CONSTRAINT [FK_Member_Of_User]
GO
ALTER TABLE [dbo].[Reserve]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_Event_Id] FOREIGN KEY([event_id])
REFERENCES [dbo].[Event] ([event_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Reserve] CHECK CONSTRAINT [FK_Reserve_Event_Id]
GO
ALTER TABLE [dbo].[Reserve]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_Room] FOREIGN KEY([room_number])
REFERENCES [dbo].[Room] ([room_number])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Reserve] CHECK CONSTRAINT [FK_Reserve_Room]
GO
ALTER TABLE [dbo].[Sign_Up]  WITH CHECK ADD  CONSTRAINT [FK_Sign_Up_event_id] FOREIGN KEY([event_id])
REFERENCES [dbo].[Event] ([event_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sign_Up] CHECK CONSTRAINT [FK_Sign_Up_event_id]
GO
ALTER TABLE [dbo].[Sign_Up]  WITH CHECK ADD  CONSTRAINT [FK_Sign_Up_User] FOREIGN KEY([rose_username])
REFERENCES [dbo].[User] ([rose_username])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sign_Up] CHECK CONSTRAINT [FK_Sign_Up_User]
GO
ALTER TABLE [dbo].[Subscribe]  WITH CHECK ADD  CONSTRAINT [FK_Subscribe_Club] FOREIGN KEY([club_name])
REFERENCES [dbo].[Club] ([club_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Subscribe] CHECK CONSTRAINT [FK_Subscribe_Club]
GO
ALTER TABLE [dbo].[Subscribe]  WITH CHECK ADD  CONSTRAINT [FK_Subscribe_User] FOREIGN KEY([rose_username])
REFERENCES [dbo].[User] ([rose_username])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Subscribe] CHECK CONSTRAINT [FK_Subscribe_User]
GO
/****** Object:  StoredProcedure [dbo].[addUserToClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addUserToClub]
	(@username varchar(10),
	@clubName varchar(50)
	)
AS
INSERT INTO dbo.Member_Of ([rose_username], [club_name])
VALUES (@username, @clubName)

GO
/****** Object:  StoredProcedure [dbo].[cancelEvent]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[cancelEvent]
	(@event_id int)
AS
DELETE FROM Event WHERE event_id = @event_id
DELETE FROM Hold WHERE event_id = @event_id
DELETE FROM Room WHERE event_id = @event_id
DELETE FROM Reserve WHERE event_id = @event_id

GO
/****** Object:  StoredProcedure [dbo].[checkIsOfficer]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[checkIsOfficer]
	(@username varchar(10),
	@clubName varchar(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM dbo.[Manage]
	WHERE rose_username = @username AND club_name = @clubName;
END


GO
/****** Object:  StoredProcedure [dbo].[createClubEvent]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[createClubEvent]
	(@clubName varchar(10),
	@event_id int,
	@eventTitle varchar(100),
	@eventDescription varchar(200),
	@startTime smalldatetime,
	@endTime smalldatetime,
	@roomNumber varchar(20),
	@bulding varchar(20)
	)
AS
INSERT INTO Event VALUES (@event_id, @eventTitle, @eventDescription)
INSERT INTO Hold VALUES (@clubName, @event_id)
INSERT INTO Room VALUES (@roomNumber, @bulding)
INSERT INTO Reserve VALUES (@startTime, @endTime, @roomNumber, @bulding)

GO
/****** Object:  StoredProcedure [dbo].[createNewUser]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[createNewUser]
	(@username varchar(10),
	@name varchar(70),
	@email varchar(30)
	)
AS
SET NOCOUNT ON;

INSERT INTO dbo.[User]
	([rose_username], name, email)
VALUES (@username, @name, @email)	

SELECT rose_username, name, email
FROM dbo.[User]
WHERE rose_username = @username
GO
/****** Object:  StoredProcedure [dbo].[deleteClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[deleteClub]
	(
	@clubName varchar(50)
	)
AS
DELETE FROM dbo.Club WHERE club_name = @clubName

GO
/****** Object:  StoredProcedure [dbo].[demoAddClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[demoAddClub]
	(@clubName varchar(10),
	@clubType varchar(16),
	@description varchar(200)
	)
AS
INSERT INTO Club VALUES(@clubName, @clubType, @description)


GO
/****** Object:  StoredProcedure [dbo].[fetchAllEvents]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchAllEvents]
AS
SELECT e.event_id, e.title, e.[description], h.club_name
FROM dbo.Event e, dbo.Hold h
WHERE e.event_id = h.event_id

GO
/****** Object:  StoredProcedure [dbo].[fetchAllRooms]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchAllRooms]
AS
SELECT dbo.Room.room_number, building, time_start, time_end, title
FROM dbo.Event LEFT OUTER JOIN dbo.Room ON dbo.Event.event_id = dbo.Room.event_id LEFT OUTER JOIN dbo.Reserve ON dbo.Room.event_id = dbo.Reserve.event_id

GO
/****** Object:  StoredProcedure [dbo].[fetchClubNames]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchClubNames]
AS
SELECT club_name
FROM dbo.Club


GO
/****** Object:  StoredProcedure [dbo].[fetchClubs]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fetchClubs]
AS
SELECT DISTINCT c.*, mem.rose_username, sub.rose_username, m.rose_username, u.name, u.email, m.title
FROM dbo.[Club] c LEFT OUTER JOIN dbo.[Manage] m
ON c.club_name = m.club_name LEFT OUTER JOIN dbo.[User] u ON u.rose_username = m.rose_username
LEFT OUTER JOIN dbo.[Member_Of] mem ON mem.club_name = c.club_name
LEFT OUTER JOIN dbo.[Subscribe] sub ON sub.club_name = c.club_name
ORDER BY c.club_name ASC

GO
/****** Object:  StoredProcedure [dbo].[fetchEvent]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchEvent]
	(@event_id int)
AS
SELECT e.*, s.rose_username
FROM dbo.[Event] e, dbo.[Sign_Up] s
WHERE e.event_id = @event_id AND s.event_id = e.event_id

GO
/****** Object:  StoredProcedure [dbo].[fetchEventByID]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[fetchEventByID]
	(@event_id int)
AS
SELECT u.rose_username, u.name, u.email
FROM dbo.[Sign_Up] s, dbo.[User] u
WHERE event_id = @event_id AND s.rose_username = u.rose_username

GO
/****** Object:  StoredProcedure [dbo].[fetchEventsByClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchEventsByClub]
	(@clubName varchar(50))
AS
SELECT e.*
FROM dbo.Hold h JOIN dbo.Event e ON h.event_id = e.event_id
WHERE h.club_name = @clubName

GO
/****** Object:  StoredProcedure [dbo].[fetchEventsForUser]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[fetchEventsForUser]
	(@username varchar(10))
AS
SELECT e.title
FROM dbo.[Event] e, dbo.[Sign_Up] s
WHERE s.rose_username = @username AND s.event_id = e.event_id	

GO
/****** Object:  StoredProcedure [dbo].[fetchEventsSignedUp]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[fetchEventsSignedUp]
	(@clubName varchar(50))
AS
SELECT u.rose_username, u.name, u.email, s.event_id
FROM dbo.[Hold] h, dbo.[Sign_Up] s, dbo.[User] u
WHERE h.club_name = @clubName AND h.event_id = s.event_id AND s.rose_username = u.rose_username

GO
/****** Object:  StoredProcedure [dbo].[fetchRegisteredUser]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fetchRegisteredUser]
	@username varchar(10)
AS
SELECT rose_username, name, email 
FROM dbo.[User_View]
WHERE rose_username = @username

GO
/****** Object:  StoredProcedure [dbo].[fetchSomeRooms]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchSomeRooms]
	(
	@roomNumber varchar(10),
	@date smallDateTime
	)
AS
DECLARE @threedaysbefore smallDateTime;
DECLARE @threedaysafter smallDateTime;
SELECT @threedaysafter = DATEADD(day, 3, @date)
SELECT dbo.Reserve.time_start, dbo.Reserve.time_end, title
FROM (dbo.Event JOIN dbo.Room ON dbo.Event.event_id = dbo.Room.event_id) JOIN dbo.Reserve ON dbo.Event.event_id = dbo.Reserve.event_id
WHERE dbo.Room.room_number = @roomNumber AND dbo.Reserve.room_number = @roomNumber AND (time_start between @threedaysbefore AND @threedaysafter)

GO
/****** Object:  StoredProcedure [dbo].[fetchUserClubMember]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[fetchUserClubMember]
	(@username varchar(10))
AS
SELECT club_name
FROM dbo.[Member_Of]
WHERE rose_username = @username


GO
/****** Object:  StoredProcedure [dbo].[fetchUserClubs]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchUserClubs]
	(@username varchar(10))
AS
SELECT dbo.Club.club_name, club_type, description
FROM (dbo.Club JOIN dbo.Member_Of ON dbo.Club.club_name = dbo.Member_Of.club_name) JOIN dbo.Subscribe ON dbo.Subscribe.club_name = dbo.Member_Of.club_name
WHERE dbo.Member_Of.rose_username = @username AND dbo.Subscribe.rose_username = @username

GO
/****** Object:  StoredProcedure [dbo].[fetchUserClubSubscribe]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[fetchUserClubSubscribe]
	(@username varchar(10))
AS
SELECT club_name
FROM dbo.[Subscribe]
WHERE rose_username = @username


GO
/****** Object:  StoredProcedure [dbo].[fetchUserEvents]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fetchUserEvents]
	(@username varchar(10))
AS
SELECT e.*, h.club_name, 
CASE WHEN s.rose_username = @username AND s.event_id = e.event_id THEN 1 ELSE 0 END
FROM dbo.[Event] e, dbo.[Sign_Up] s, dbo.[Hold] h
WHERE h.event_id = e.event_id AND s.rose_username = @username
GO
/****** Object:  StoredProcedure [dbo].[fetchUserOfficer]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[fetchUserOfficer]
	(@username varchar(10))
AS
SELECT club_name, title
FROM dbo.[Manage]
WHERE rose_username = @username


GO
/****** Object:  StoredProcedure [dbo].[getAllFilesByClubName]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getAllFilesByClubName]
	@clubName varchar(50)
AS
SELECT f.[file_id], f.[file_path], f.[file_type]
FROM dbo.[Media File] f
WHERE f.club_name = @clubName

GO
/****** Object:  StoredProcedure [dbo].[getAllManagersByClubName]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getAllManagersByClubName]
	@clubName varchar(50)
AS
SELECT u.rose_username, u.name, u.email, m.title
FROM dbo.[Manage] m, dbo.[User] u
WHERE m.club_name = @clubName AND m.rose_username = u.rose_username

GO
/****** Object:  StoredProcedure [dbo].[getAllMembersByClubName]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getAllMembersByClubName]
	@clubName varchar(50)
AS
SELECT u.rose_username, u.name, u.email
FROM dbo.[Member_Of] m, dbo.[User] u
WHERE m.club_name = @clubName AND m.rose_username = u.rose_username

GO
/****** Object:  StoredProcedure [dbo].[getAllSubscribersByClubName]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getAllSubscribersByClubName]
	@clubName varchar(50)
AS
SELECT u.rose_username, u.name, u.email
FROM dbo.[Subscribe] s, dbo.[User] u
WHERE s.club_name = @clubName AND s.rose_username = u.rose_username

GO
/****** Object:  StoredProcedure [dbo].[getClubFromEvent]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getClubFromEvent]
	(@event_id int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT club_name
	FROM dbo.Hold
	WHERE event_id = @event_id
END


GO
/****** Object:  StoredProcedure [dbo].[getClubInformation]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getClubInformation]
	@clubName varchar(50)
AS
SELECT c.club_name, c.club_type, c.description, mem.rose_username, sub.rose_username, m.rose_username, u.name, u.email, m.title, mf.file_path
FROM dbo.[Club] c LEFT OUTER JOIN dbo.[Manage] m ON c.club_name = m.club_name 
LEFT OUTER JOIN dbo.[User] u ON m.rose_username = u.rose_username
LEFT OUTER JOIN dbo.Member_Of mem ON mem.club_name = c.club_name 
LEFT OUTER JOIN dbo.Subscribe sub ON sub.club_name = c.club_name
LEFT OUTER JOIN dbo.[Media File] mf ON mf.club_name = c.club_name
WHERE c.club_name = @clubName AND mem.rose_username = sub.rose_username
GO
/****** Object:  StoredProcedure [dbo].[getOneClubByClubName]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getOneClubByClubName]
	@clubName varchar(50)
AS
SELECT c.[club_name], c.[club_type], c.[description]
FROM dbo.[Club] c
WHERE c.club_name = @clubName

GO
/****** Object:  StoredProcedure [dbo].[getUserRegistered]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getUserRegistered]
	@username varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT rose_username
	FROM dbo.[User]
	WHERE rose_username = @username;
END

GO
/****** Object:  StoredProcedure [dbo].[removeUserFromEvent]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[removeUserFromEvent]
	(@username varchar(10),
	@event_id int
	)
AS
DELETE FROM Sign_Up WhERE rose_username = @username AND event_id = @event_id


GO
/****** Object:  StoredProcedure [dbo].[signUserUpForEvent]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[signUserUpForEvent]
	(@username varchar(10),
	@event_id int
	)
AS
INSERT INTO Sign_Up VALUES(@username, @event_id)


GO
/****** Object:  StoredProcedure [dbo].[subscribeUserToClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[subscribeUserToClub]
	(@username varchar(10),
	@clubName varchar(50)
	)
AS
INSERT INTO dbo.Subscribe([rose_username], [club_name])
VALUES (@username, @clubName)

GO
/****** Object:  StoredProcedure [dbo].[unsignUpForClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[unsignUpForClub]
	(@username varchar(10),
	@clubName varchar(50)
	)
AS
DELETE FROM dbo.Member_Of
WHERE rose_username = @username AND club_name = @clubName

GO
/****** Object:  StoredProcedure [dbo].[unsubscribeClub]    Script Date: 2/17/2017 5:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[unsubscribeClub]
	(@username varchar(10),
	@clubName varchar(50)
	)
AS
DELETE FROM dbo.Subscribe
WHERE rose_username = @username AND club_name = @clubName

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Manage"
            Begin Extent = 
               Top = 23
               Left = 67
               Bottom = 236
               Right = 343
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Club_in_Charge_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Club_in_Charge_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Club"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 225
               Right = 351
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Subscribe"
            Begin Extent = 
               Top = 12
               Left = 779
               Bottom = 191
               Right = 1055
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Member_Of"
            Begin Extent = 
               Top = 12
               Left = 427
               Bottom = 191
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Manage"
            Begin Extent = 
               Top = 12
               Left = 1131
               Bottom = 225
               Right = 1407
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2445
         Alias = 1298
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Club_List_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Club_List_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Event"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 227
               Right = 351
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Reserve"
            Begin Extent = 
               Top = 0
               Left = 540
               Bottom = 247
               Right = 815
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Room"
            Begin Extent = 
               Top = 12
               Left = 891
               Bottom = 191
               Right = 1166
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sign_Up_Events_View"
            Begin Extent = 
               Top = 12
               Left = 1242
               Bottom = 259
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User_View"
            Begin Extent = 
               Top = 12
               Left = 1594
               Bottom = 225
               Right = 1870
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
    ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Event_List_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Event_List_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Event_List_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 225
               Right = 352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Member_Of_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Member_Of_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 225
               Right = 352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Member_Of"
            Begin Extent = 
               Top = 12
               Left = 428
               Bottom = 191
               Right = 704
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Member_Or_Subscribe_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Member_Or_Subscribe_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 225
               Right = 352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Event"
            Begin Extent = 
               Top = 12
               Left = 428
               Bottom = 225
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Sign_Up_Events_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Sign_Up_Events_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Sign_Up_1"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 191
               Right = 352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Sign_Up_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Sign_Up_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 225
               Right = 352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Subscribe_To_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Subscribe_To_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Sign_Up"
            Begin Extent = 
               Top = 30
               Left = 55
               Bottom = 209
               Right = 331
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Subscribe_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Subscribe_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 225
               Right = 352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'User_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'User_View'
GO
USE [master]
GO
ALTER DATABASE [RHIT_Club_App] SET  READ_WRITE 
GO
